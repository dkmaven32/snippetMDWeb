function drawChart(input, element_id) {
	green = "#a5dc5b";
	orange = "#feb421";
	red = "#fc292b";
	
	// greenLimit = 100;
	orangeLimit = 220;
	redLimit = 280;
	
	chartData = [ ['Year', 'Cholesterol', {'type': 'string', 'role': 'style'}] ];
	for( i=0; i < input.length; i++){
		color = green;
		if(input[i][1] > orangeLimit) color = orange;
		if(input[i][1] > redLimit) color = red;
		el = [input[i][0], input[i][1], 'point { size: 3; shape-type: circle; fill-color: '+color+' }'];
		chartData.push(el);
	}
	
	var data = google.visualization.arrayToDataTable(chartData);

	var options = {
		height:100,
		legend: 'none',
		'chartArea': {'width': '100%'},
		//vAxis: { minValue: 100},
		//curveType: 'function',
		colors:["#cccccc"],
		pointSize: 3,
		<!-- dataOpacity: 0.3, -->
		hAxis: {
			gridlines: {color: '#ccc'},
			 textStyle : {
				fontSize: 8 // or the number you want
			}
		},
		vAxis: {
			minValue: 0, 
			viewWindow:{min:0}, 
			gridlines: { lineDashStyle: [1,1]},
			 baselineColor: '#ccc',
			 gridlineColor: '#fff',
			 textPosition: 'none'
		}
	};

	var chart = new google.visualization.LineChart(document.getElementById(element_id));
	chart.draw(data, options);
}