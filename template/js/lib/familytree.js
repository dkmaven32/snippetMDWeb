var config = {
        container: "#FamilyHistoryTree",
        levelSeparation:    70,
		rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
        siblingSeparation:  60,
        nodeAlign: "BOTTOM",
        connectors: {
            type: "step",
            style: {
                "stroke-width": 1,
                "stroke": "#ccc",
                "stroke-dasharray": "_", //"", "-", ".", "-.", "-..", ". ", "- ", "--", "- .", "--.", "--.."
                "arrow-end": "classic-wide-long"
            }
        }
    },

    family_root = {
        innerHTML: "#family-root"
    },

    family_f = {
        parent: family_root,
        innerHTML: "#family-f"
    },
	family_f_f = {
        parent: family_f,
        innerHTML: "#family-f-f"
    },
	family_f_m = {
        parent: family_f,
        innerHTML: "#family-f-m"
    },
    family_m = {
        parent: family_root,
        innerHTML: "#family-m"
    }

    family_m_f = {
        parent: family_m,
        innerHTML: "#family-m-f"
    },
	family_m_m = {
        parent: family_m,
        innerHTML: "#family-m-m"
    },

tree_structure = [
    config, family_root, family_f, family_f_f,family_f_m, family_m, family_m_f,family_m_m
];


