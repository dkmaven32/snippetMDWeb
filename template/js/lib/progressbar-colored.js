function setColoredProgressbar(p, progressbar_id){
	
	//Configuring start/middle/end color for the progress bar:...
	var gr = new GradientReader([{stop: 0.0, color: '#f00'},
						 {stop: 0.5, color: '#ff0'},
						 {stop: 1.0, color: 'rgb(79,189,0)'}]);
	
	//Getting DOM elements for the progressbar:...
	prog = document.getElementById(progressbar_id).getElementsByClassName('snip-progress-bar')[0];
	plabel = document.getElementById(progressbar_id).getElementsByClassName('snip-progress-label')[0];
	
	width = prog.offsetWidth; //bar
	pwidth = plabel.offsetWidth; //label
	
	//Calculate the percentages
	label_percentage = pwidth/width; //label width percentage on the whole progress bar
	start_percentage = p - (label_percentage/2); //on the start point of the label
	end_percentage = p + (label_percentage/2); //on the end point of the label
	
	//Move the label:
	dist = width*start_percentage;
	plabel.style.left = dist+"px";
	
	//Calculating the start/middle/end color:
	col = gr.getColor(start_percentage*100);
	col1 = 'rgba(' + col[0] + ',' + col[1] + ',' + col[2] + ',1)';
	col = gr.getColor(p*100);
	col2 = 'rgba(' + col[0] + ',' + col[1] + ',' + col[2] + ',1)';
	col = gr.getColor(end_percentage*100);
	col3 = 'rgba(' + col[0] + ',' + col[1] + ',' + col[2] + ',1)';
	
	//Setting label color:
	plabel.style.background = "-moz-linear-gradient(left, "+col1+" 0%,  "+col2+" 50%, "+col3+" 100%)";
}

// our object
function GradientReader(colorStops) {

	var canvas = document.createElement('canvas'),
		ctx = canvas.getContext('2d'),
		gr = ctx.createLinearGradient(0, 0, 101, 0),
		i = 0, cs;
	
	canvas.width = 101;
	canvas.height = 1;
	
	for(; cs = colorStops[i++];)
		gr.addColorStop(cs.stop, cs.color);
	
	ctx.fillStyle = gr;
	ctx.fillRect(0, 0, 101, 1);
	
	this.getColor = function(pst) {
		return ctx.getImageData(pst|0, 0, 1, 1).data;
	};
}


function setVbarProgress(){
	vbars = document.getElementsByClassName('vbar');
	for(i=0; i< vbars.length; i++){
		vbar = vbars[i];
		percentage = vbar.getAttribute("data-progress");
		max =vbar.getAttribute("data-maxval");
		h = max * percentage/100;
		vbar.style.height=h+"px";
	}
}
	