import React from 'react';
import { IndexLink, Link } from 'react-router';

class SidebarSettings extends React.Component {
	 constructor(props) {
		super(props);
		this.state = { 
			infoCollapsed: false,
			checkCollapsed: true,
		};
	}
	
	collapse(event){
		var obj = {};
		obj[event.target.getAttribute("data-collapse")] = !this.state[event.target.getAttribute("data-collapse")]; 
		this.setState(obj);
	}
	componentDidMount(){
		$(".date-checkpoint").datepicker("setDate", new Date(2017,3,21));
		$(".date-checkpoint").datepicker('update');
	}
	
  render() {
    const active = { borderBottomColor: '#3f51b5' };
	const activeMenu = "menu-item menu-item-active";
	const deactiveMenu = "menu-item";
	
	
	const triangleDown = "fa fa-caret-down text";
	const triangleUp = "fa fa-caret-up text";
	
	const displayInfo ="sidebox-body patient-profile-body";
	const hideInfo ="sidebox-body patient-profile-body hidden";
	
    return (
		<aside className="bg-darkgreen aside-md colbox" id="sidebar">
			<div className="sidebox">
				<div className="sidebox-head head-border">
					<p className="text-medium">Next Patient 
						<span className="pull-right">
							<span className="text-lgreen" >1:30pm</span>
						</span>
					</p>
				</div>
				<div className="clearfix"></div>
			</div>
			<div className="sidebox">
				<div className="sidebox-head">
					<div className="patient-img-container pull-left">
						<img src="images/patients/patient1.png" />
					</div>
					
					<div className="patient-info">
						<p className="text-medium">Michael Lui</p>
						<div className="text-smallx">DOB: 11&#47;17&#47;1980 
							<ul className="nav nav-pills pull-right">
								<li>
									<span className="a-pointer panel-toggle" data-collapse="infoCollapsed" onClick = { this.collapse.bind(this) } >
										<i className={this.state.infoCollapsed ? triangleDown : triangleUp }></i>
									</span>
								</li>
							</ul>
						</div>
						<p className="patient-measurement"><span className="text-lgreen" >H: 74&rdquo;, W: 169lbs &nbsp;BP: 118&#47;70</span></p>
					</div>
				</div>
				<div className="clearfix"></div>
				<div className={this.state.infoCollapsed ? hideInfo: displayInfo}>
					<div className="contact-info">
						<p className="text-smallx">mike@email.com</p>
						<p className="text-smallx">206-288-3588</p>
					</div>
					<div className="text-smallx">
						Mike is a 36-year-old nonsmoker who has three present conditions and one high risk. We are treating him with medicine, monitoring, and a physical activity regimen. His lab results are normal when compared to patients with his age, height, and weight.
					</div>
				</div>
			</div>
			
			
			<div className="sidebox">
				<div className="sidebox-body patient-menu">
					<div className={this.props.activeMenu == 'settings' ? activeMenu : deactiveMenu}>
						<Link to="/settings" ><img src="images/patient_profile_white.png" />Settings</Link>
					</div>
					<div className={this.props.activeMenu == 'mypatientlist' ? activeMenu : deactiveMenu}>
						<Link to="/settings" ><img src="images/heart_white.png" />Feedback</Link>
					</div>
				</div>
			</div>
			
		</aside>
    );
  }
}

export default SidebarSettings;
