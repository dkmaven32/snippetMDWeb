import React from 'react';
import { IndexLink, Link } from 'react-router';

class Header extends React.Component {
  render() {
    const active = { borderBottomColor: '#3f51b5' };
    return (
		<div>
			<header className="top-bar bg-dark navbar navbar-static-top" >
				<div className="container-fluid">
					<div id="top-navbar" className="navbar-right navbar-collapse collapse" >
					   <ul className="nav navbar-nav">
							<li >
								<Link to="/settings" className="nav-active" >Settings</Link>
							</li>
							<li ><a href="/help" >Help</a></li>
							<li ><a href="/logout" >Logout</a></li>
					   </ul>
					</div>
				</div>
			</header>
			<header className="administrative-bar bg-darker navbar navbar-static-top" >
				
				<div className="container-fluid" >
					<div className="navbar-header aside-md">
						<div className="logo-container">
							<Link to="/patientprofile" className="navbar-brand" >
								<img src="images/logo.png" />
								<span className="logo-text">Snippet MD</span>
							</Link>
						</div>
					</div>
					<ul className="nav navbar-nav m-n">
						<li className="administrative-item" >
							<Link to="/mypatientlist" ><img src="images/mypatients_white.png" />My Patients</Link>
						</li>
						<li className="administrative-item">
							<div className="searchbar-container">
								<input className="searchbar" placeholder="Find patients by name" value="" />
								<img src="images/search_icon.png" className="search-icon" />
							</div>
						</li>
					</ul>
					
					<ul className="nav navbar-nav navbar-right m-n">
						<li id="user-info">
							<div className="thumb-sm avatar pull-left">
								<img src="images/doctors/doctor-06.jpg" />
							</div>
							<div className="gen-info">
								<p className="text-medium">Dr. David Smith</p>
								<p className="text-small">Martin Luther King, Jr. Community Hospital</p>
							</div>
						</li>
						<li className="administrative-alert">
							<img src="images/notification_alert.png" />
						</li>
					</ul>
				</div>
			</header>
		</div>
    );
  }
}

export default Header;
