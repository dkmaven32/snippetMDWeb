import React from 'react';
import Genetics from './SupportingData/Genetics';
import FamilyHistory from './SupportingData/FamilyHistory';
import LabTests from './SupportingData/LabTests';
import Nutrition from './SupportingData/Nutrition';
import PhysicalActivity from './SupportingData/PhysicalActivity';
import { IndexLink, Link } from 'react-router';

class SupportingData extends React.Component {
  
  constructor(props) {
		super(props);
		this.state = { 
			Genetics:{
				isVisible: true 
			},
			LabTests:{
				isVisible: true 
			},
			PhysicalActivity:{
				isVisible: false 
			},
			Nutrition:{
				isVisible: false 
			},
			FamilyHistory:{
				isVisible: false 
			},
		};
	}

	toggle(event) {
		var obj = {};
		obj[event.target.getAttribute("data-section")] = {isVisible: !this.state[event.target.getAttribute("data-section")].isVisible}; 
		this.setState( obj );
	}
  
  render() {
	  var primaryCls = 'btn btn-primary';
	  var defaultCls = 'btn btn-default';
	  
    return this.props.data ? (
		<section>
			<div className="row">
				<div className="col-md-12">
					<h4 className="title-section pull-left">Supporting Data</h4>
					<span className="pull-right text-small">Save preset <input type="checkbox" name="preset" value="1"/></span>
				</div>
			</div>
			<div className="button-row">
				<span data-section='Genetics' className={this.state.Genetics.isVisible ? primaryCls : defaultCls} onClick={this.toggle.bind(this)} >Genetics</span>
				<span data-section='LabTests' className={this.state.LabTests.isVisible ? primaryCls : defaultCls}  onClick={this.toggle.bind(this)} >Lab Tests</span>
				<span data-section='PhysicalActivity' className={this.state.PhysicalActivity.isVisible ? primaryCls : defaultCls}  onClick={this.toggle.bind(this)} >Physical Activity</span>
				<span data-section='Nutrition' className={this.state.Nutrition.isVisible ? primaryCls : defaultCls}  onClick={this.toggle.bind(this)} >Nutrition</span>
				<span data-section='FamilyHistory' className={this.state.FamilyHistory.isVisible ? primaryCls : defaultCls}  onClick={this.toggle.bind(this)} >Family History</span>
			</div>
			
			<Genetics visibility={this.state.Genetics.isVisible} data={this.props.data.genetics} />
			<FamilyHistory  visibility={this.state.FamilyHistory.isVisible} data={this.props.data.family_history} />
			<LabTests  visibility={this.state.LabTests.isVisible} data={this.props.data.lab_tests} />
			<Nutrition  visibility={this.state.Nutrition.isVisible} />
			<PhysicalActivity  visibility={this.state.PhysicalActivity.isVisible} />
		</section>
    ) : null;
  }
}

export default SupportingData;
