import React from 'react';

class MedicalJournal extends React.Component {
  constructor(props) {
		super(props);
		this.state = { 
			Collapsed: false
		};
	}
	
	collapse(event){
		this.setState({Collapsed: !this.state.Collapsed});
	}
	
	render() {
		
		var collapsedCls = "text-muted hidden";
		var relapsedCls = "text-muted";
		
		var triangleDown = "fa fa-caret-down text";
		var triangleUp = "fa fa-caret-up text";
		
		return (
			<div>
				<h4 className="title-section">MEDICAL JOURNAL</h4>
				<table className="table-panel vertical-middle table-panel-bordered table-panel-4x">
					<thead>
						<tr>
							<th>Conditions</th>
							<th>
								Patients Goals <img src="images/plus_circle.png" className="m15-l" />
							</th>
							<th>
								Treatment Summary
							</th>
							<th>
								Risks
								<ul className="nav nav-pills pull-right">
									<li>
										<a href="#" className="panel-toggle" onClick = { this.collapse.bind(this) } >
											<i className={this.state.Collapsed ? triangleDown : triangleUp }></i>
										</a>
									</li>
								</ul>
								
							</th>
						</tr>
					</thead>
					<tbody className={this.state.Collapsed ? collapsedCls : relapsedCls }>
						<tr>
							<td rowSpan="6" >
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Hypertension <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 08/15/2013</p>
								</div>
								
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Atrial Fibrillation <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 02/02/2011</p>
								</div>
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Hypoglycemia <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 06/29/2010</p>
								</div>
							</td>
							<td rowSpan="3">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
							<td rowSpan="6">
								<p className="text-medium text-gray font-semibold">Venous Tromboembolism <i className="fa fa-info-circle"></i></p>
								<p className="text-muted text-italic">High risk</p>
							</td>
						</tr>
						<tr>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
						</tr>
						<tr>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
						</tr>
						<tr>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
							<td>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
						</tr>
						<tr>
							<td rowSpan="2">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
							<td >
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
						</tr>
						<tr>
							<td >
								Lorem ipsum dolor sit amet, consectetur adipisicing elit sed
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

export default MedicalJournal;
