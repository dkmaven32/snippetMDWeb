import React from 'react';

class MedicalJournal extends React.Component {
  constructor(props) {
		super(props);
		this.state = { 
			Collapsed: false
		};
	}
	
	collapse(event){
		this.setState({Collapsed: !this.state.Collapsed});
	}
	
	render() {
		
		var collapsedCls = "text-muted hidden";
		var relapsedCls = "text-muted";
		
		var triangleDown = "fa fa-caret-down text";
		var triangleUp = "fa fa-caret-up text";
		
		return (
			<div>
				<h4 className="title-section">MEDICAL JOURNAL</h4>
				<table className="table-panel vertical-middle table-panel-bordered table-panel-4x">
					<thead>
						<tr>
							<th>Conditions</th>
							<th>
								Patients Goals <img src="images/plus_circle.png" className="m15-l" />
							</th>
							<th>
								Risks
								<ul className="nav nav-pills pull-right">
									<li>
										<span className="a-pointer panel-toggle" onClick = { this.collapse.bind(this) } >
											<i className={this.state.Collapsed ? triangleDown : triangleUp }></i>
										</span>
									</li>
								</ul>
								
							</th>
						</tr>
					</thead>
					<tbody className={this.state.Collapsed ? collapsedCls : relapsedCls }>
						<tr>
							<td>
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Hypertension <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 08/15/2013</p>
								</div>
								
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Atrial Fibrillation <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 02/02/2011</p>
								</div>
								<div className="list-item">
									<p className="text-medium text-gray font-semibold">Hypoglycemia <i className="fa fa-info-circle"></i></p>
									<p className="text-muted text-italic">First diagnosed 06/29/2010</p>
								</div>
							</td>
							<td>
								<ul className="m-n">
									<li className="bulleted text-muted m-b">Reduce sodium intake to no more than 1,800mg daily</li>
									<li className="bulleted text-muted m-b">Perform 30 minutes of moderate activity 5x a week</li>
									<li className="bulleted text-muted m-b">Limit alcohol consumption to 1 drink daily</li>
									<li className="bulleted text-muted m-n">Continue taking ACE inhibitor 1x daily</li>
								</ul>
							</td>
							<td>
								<p className="text-medium text-gray font-semibold">Coronary Heart Disease <i className="fa fa-info-circle"></i></p>
								<p className="text-muted text-italic">High risk</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

export default MedicalJournal;
