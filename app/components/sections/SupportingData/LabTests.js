import React from 'react';

class LabTests extends React.Component {
  componentDidMount(){
	$(".cameleon-calendar-year").datepicker({
		autoclose: true,
		format: " yyyy",
		viewMode: "years", 
		minViewMode: "years",
		startDate: '2014',
		endDate: new Date(),
	});
	
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(function(){
		var inp = [
				  ['2008', 250],
				  ['2009', 300],
				  ['2010', 270],
				  ['2011', 250],
				  ['2012', 199],
				  ['2013', 210],
				  ['2014', 290],
				  ['2015', 220],
				  ['2016', 230]
			];
		drawChart(inp, 'cholesterol-level');
		drawChart(inp, 'hdl-level');
		drawChart(inp, 'ldl-level');
		drawChart(inp, 'triglyceride-level');
	});
  }
  
  render() {
    return this.props.data ? (
      <section className={this.props.visibility == true ? 'fadein-show' : 'fadein-hide'}>
		<table className="table-panel vertical-middle table-panel-bordered">
			<thead>
				<tr>
					<th className="td-40">Lab Tests</th>
					<th className="td-35">
						Trends
						<div className="input-group date cameleon-calendar-year">
							<input type="text" className="form-control" />
							<span className="input-group-addon icon"><i className="fa fa-calendar"></i></span>
						</div>
					</th>
					<th className="td-25">
						Related Conditions & Risks 
					</th>
				</tr>
			</thead>
			<tbody className="text-muted">
				<tr>
					<td>
						{this.props.data.tests.map( (test)=> 
							<div className="list-item labtests-h" >
								<p className="text-medium text-gray font-semibold">{test.title} <span className="text-small text-italic font-regular pull-right">{test.value}</span></p>
								<p className="text-muted text-italic">{test.description}</p>
							</div>
						)}
					</td>
					<td className="arrowangle" >
						<div id="cholesterol-level"></div>
						<div id="hdl-level"></div>
						<div id="ldl-level"></div>
						<div id="triglyceride-level"></div>
					</td>
					<td>
						{this.props.data.risks.map( (risk) => <p className="text-small font-semibold m15-b">{risk.title} <i className="fa fa-info-circle"></i></p> ) }
					</td>
				</tr>
			</tbody>
		</table>
      </section>
    ) : null;
  }
}

export default LabTests;
