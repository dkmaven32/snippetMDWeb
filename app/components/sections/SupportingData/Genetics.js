import React from 'react';

class Genetics extends React.Component {
  render() {
	return this.props.data ? (
		<section className={this.props.visibility == true ? 'fadein-show' : 'fadein-hide'} >
			<div className="panel panel-default">
				<div className="panel-heading">
					<div className="row">
						<div className="col-md-3"><span className="m-r">Genetics</span><i className="fa fa-arrows"></i></div>
						<div className="col-md-4">
							<span className="m30-r text-italic text-muted font-regular"><i className="circle-yellow-x1"></i>Low</span>
							<span className="m30-r text-italic text-muted font-regular">
								<i className="circle-yellow-x5"></i>
								High confidence
							</span>
						</div>
					</div>
					
				</div>
				<div className="panel-body">
					<div className="row">
						<div className="col-md-3">
							<h3 className="text-medium text-gray">Condition / Risk</h3>
							<p className="text-smallx">Coronary Heart Disease <i className="fa fa-info-circle"></i></p>
							<i className="circle-yellow-x5"></i>
						</div>
						<div className="col-md-9">
							<div className="row">
								<div className="col-md-6">
									<h3 className="text-medium text-gray">Patient's Risk</h3>
									<div className="m30-t"></div>
									<p className="text-muted">{this.props.data.patient_risk.percentage}</p>
									<div className="m30-t"></div>
									<div className="patient-risk-desc">
										<p className="text-muted text-red">{this.props.data.patient_risk.numeric}</p>
										<p className="text-muted text-italic">
										{this.props.data.patient_risk.description}
										</p>
									</div>
								</div>
								<div className="col-md-6">
									<h3 className="text-medium text-gray">Average Risk</h3>
									<div className="m30-t"></div>
									<p className="text-muted">{this.props.data.average_risk.percentage}</p>
									<div className="m30-t"></div>
									<div className="patient-risk-desc">
										<p className="text-muted text-red">{this.props.data.average_risk.numeric}</p>
										<p className="text-muted text-italic">
										{this.props.data.average_risk.description}
										</p>
									</div>
								</div>
							</div>
							<div className="row">
								<div className="col-md-12">
									<div className="m15-t"></div>
									<h3 className="text-medium text-gray">Factors used to estimate risk</h3>
									<div className="m30-t"></div>
									<table className="table table-bordered">
										<tbody>
											<tr>
												<th className="font-semibold">Age</th>
												<th className="font-semibold">Ancestry</th>
												<th className="font-semibold">Sex</th>
												<th className="font-semibold">SNP</th>
											</tr>
											<tr>
												<td>{this.props.data.factors.age}</td>
												<td>{this.props.data.factors.ancestry}</td>
												<td>{this.props.data.factors.sex}</td>
												<td>
													{this.props.data.factors.snp}
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>
	) : null ;
	
  }
}

export default Genetics;
