import React from 'react';

class FamilyHistory extends React.Component {
  
  componentDidMount(){
	  new Treant( tree_structure );
  }
  componentWillReceiveProps (){
	  new Treant( tree_structure );
  }
  
  render() {
    return this.props.data ? (
      <section className={this.props.visibility == true ? 'fadein-show' : 'fadein-hide'}>
		<table className="table-panel vertical-middle table-panel-bordered">
			<thead>
				<tr>
					<th>Family History</th>
					<th className="td-25">
						Related Conditions & Risks
					</th>
				</tr>
			</thead>
			<tbody className="text-muted">
				<tr>
					<td className="arrowangle">
						<div style={{display:"none"}}>
							<ul>
								<li>
									<div id="family-root" className="">
									   
									</div>
									<ul>
										<li>
											<div id="family-f" className="treenode divtable">
												<div className="table-cell">
													<img alt="Father" src="images/male.png" />
												</div>
												<div className="table-cell">
													<p>
														<span className="text-muted text-italic">
															{this.props.data.father.description}
														</span>
													</p>
												</div>
											</div>
											<ul>
												<li>
													<div id="family-f-f" className="treenode divtable">
														<div className="table-cell">
															<img alt="Father" src="images/male.png"/>
														</div>
														<div className="table-cell">
															<p>
																<span className="text-muted text-italic">
																	{this.props.data.father.grandpa.description}
																</span>
															</p>
														</div>
													</div>
												</li>
												<li>
													<div id="family-f-m" className="treenode divtable">
														<div className="table-cell">
															<img alt="Mother" src="images/female.png"/>
														</div>
														<div className="table-cell">
															<p>
																<span className="text-muted text-italic">
																	{this.props.data.father.grandma.description}
																</span>
															</p>
														</div>
													</div>
												</li>
											</ul>
										</li>
										<li>
											<div id="family-m" className="treenode divtable">
												<div className="table-cell">
													<img alt="Mother" src="images/female.png"/>
												</div>
												<div className="table-cell">
													<p>
														<span className="text-muted text-italic">
															{this.props.data.mother.description}
														</span>
													</p>
												</div>
											</div>
											<ul>
												<li>
													<div id="family-m-f" className="treenode divtable">
														<div className="table-cell">
															<img alt="Father" src="images/male.png"/>
														</div>
														<div className="table-cell">
															<p>
																<span className="text-muted text-italic">
																	{this.props.data.mother.grandpa.description}
																</span>
															</p>
														</div>
													</div>
												</li>
												<li>
													<div id="family-m-m" className="treenode divtable">
														<div className="table-cell">
															<img alt="Mother" src="images/female.png"/>
														</div>
														<div className="table-cell">
															<p>
																<span className="text-muted text-italic">
																	{this.props.data.mother.grandma.description}
																</span>
															</p>
														</div>
													</div>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<div id="FamilyHistoryTree"></div>
					</td>
					<td>
						{this.props.data.risks.map( (risk) => <p className="text-small font-semibold m15-b">{risk.title} <i className="fa fa-info-circle"></i></p> )}
					</td>
				</tr>
			</tbody>
		</table>
      </section>
    ) : null;
  }
}

export default FamilyHistory;
