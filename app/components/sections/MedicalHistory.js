import React from 'react';

class MedicalHistory extends React.Component {
  render() {
	  
    return this.props.data ? (
      <section>
		<div className="row">
			<div className="col-md-12">
				<h4 className="title-section pull-left">Medical History</h4>
			</div>
		</div>
		<div className="button-row">
			<span className="text-muted label font-regular">Filter by</span>
			<span className="btn btn-primary">Date / Period</span>
			<span className="btn btn-default">Condition</span>
			<span className="btn btn-default">Type</span>
			<span className="btn btn-default">Prescriptions</span>
			<div className="pull-right searchbar-container">
				<input className="searchbar search-blue" placeholder="Search by keyword" value="" />
				<img src="images/search_icon_blue.png" className="search-icon" />
			</div>
		</div>
		<div className="timeline timeline-single-column m15-b">
			{this.props.data.medical_history.map( (item) =>
				<div>
				{item.type == "year" ? // Type of year
					<div className="timeline-item timeline-item-year" >
						<div className="timeline-point timeline-point-blank timeline-point-year">
							<p className="timeline-year">{item.title}</p>
						</div>
					</div>
				: //Type activity
					<div className="timeline-item timeline-item-arrow-sm">
						<div className="timeline-point timeline-point-primary">
							{item.type == 'physical'? <img src="images/chemistry.png"/>
							: item.type == 'medicine' ? <img src="images/aspirin.png"/>
								: item.type == 'medicine' ? <img src="images/aspirin.png"/>
									: item.type == 'surgery' ? <img src="images/picker.png"/>
										: <img src="images/starer.png"/>
							}
						</div>
						<div className="timeline-event timeline-event-primary">
							<div className="well">
								<p className="text-medium text-blue font-semibold">{item.title}</p>
								<div className="row text-muted text-italic">
									<div className="col-md-1">{item.datetime}</div>
									<span className="col-md-11">{item.description}</span>
								</div>
							</div>
						</div>
					</div>
				}
				</div>
			)}
			
		</div>
      </section>
    ) : null ;
  }
}

export default MedicalHistory;
