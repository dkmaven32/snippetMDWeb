import React from 'react';

import Header from './Header';
import Footer from './Footer';

import SidebarSettings from './SidebarSettings';

class Settings extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"settings",
			patient:{}
		};
	}
	
	componentDidMount(){
		
		//Rendering rangers:
		console.log("CHECK here");
		
		var hardstop = document.getElementById('hardstop');
		var required_override_reason = document.getElementById('required_override_reason');
		var optional_override_reason = document.getElementById('optional_override_reason');
		var renderLabel = function(target, minVal, maxVal, type){
			minVal = parseInt(minVal);
			maxVal = parseInt(maxVal);
			
			console.log("MinVal",minVal);
			console.log("MaxVal",maxVal);
			
			var lk_label = {
				1:"I",
				2:"II",
				3:"III",
				4:"IV",
				5:"V",
				6:"VI",
				7:"VII",
			};
			
			var text = "";
			if( (maxVal - minVal) == 1 ){
				text = lk_label[maxVal]+" only";
			}else if((maxVal - minVal) > 1){
				minVal++;
				text = lk_label[minVal]+"-"+lk_label[maxVal];
			}
			
			$(target+" .noUi-connect").attr("data-aftercontent", text);
			
		}
		
		noUiSlider.create(hardstop, {
			connect: [false, true],
			step: 1,
			behaviour: 'tap',
			start: 5,
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#hardstop", 5, 7);
		
		noUiSlider.create(required_override_reason, {
			connect: true,
			step: 1,
			behaviour: 'tap',
			start: [ 4, 5 ],
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#required_override_reason", 4,5);
		
		noUiSlider.create(optional_override_reason, {
			connect: [true, false],
			step: 1,
			behaviour: 'tap',
			start: 4 ,
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#optional_override_reason", 0, 4);
		
		
		hardstop.noUiSlider.on('change', function(){
			
			var val = hardstop.noUiSlider.get();
			required_override_reason.noUiSlider.set([null, val]);
			
			renderLabel("#hardstop", val, 7);
			
			var vals = required_override_reason.noUiSlider.get();
			console.log("CHECK vals:", vals);
			if(vals[0] > val ){
				vals[1] = vals[0] = val;
				required_override_reason.noUiSlider.set([val, val]);
			}
			
			renderLabel("#required_override_reason", vals[0], vals[1]);
			
			
		});
		
		required_override_reason.noUiSlider.on('change', function(){
			var vals = required_override_reason.noUiSlider.get();
			var startVal= vals[0];
			var endVal 	= vals[1];
			hardstop.noUiSlider.set(endVal);
			
			renderLabel("#required_override_reason", vals[0], vals[1]);
			renderLabel("#hardstop", endVal, 7);
			
			optional_override_reason.noUiSlider.set(startVal);
			renderLabel("#optional_override_reason", 0, startVal);
			
		});
		
		required_override_reason.noUiSlider.on('set', function(){
			var vals = required_override_reason.noUiSlider.get();
			var startVal= vals[0];
			var endVal 	= vals[1];
			hardstop.noUiSlider.set(endVal);
			
			renderLabel("#required_override_reason", vals[0], vals[1]);
			renderLabel("#hardstop", endVal, 7);
			
			optional_override_reason.noUiSlider.set(startVal);
			renderLabel("#optional_override_reason", 0, startVal);
			
		});
		
		optional_override_reason.noUiSlider.on('change', function(){
			var val = optional_override_reason.noUiSlider.get();
			
			renderLabel("#optional_override_reason", 0, val);
			
			required_override_reason.noUiSlider.set(val);
			var vals = required_override_reason.noUiSlider.get();
			if(vals[0] > vals[1] ){
				vals[1] = vals[0] = val;
				required_override_reason.noUiSlider.set([val, val]);
			}
			renderLabel("#required_override_reason", vals[0], vals[1]);
			
		});
		
		
		var inlinetext = document.getElementById('inlinetext');
		var indicator_only = document.getElementById('indicator_only');
		var noalert = document.getElementById('noalert');
		var renderLabel = function(target, minVal, maxVal, type){
			minVal = parseInt(minVal);
			maxVal = parseInt(maxVal);
			
			console.log("MinVal",minVal);
			console.log("MaxVal",maxVal);
			
			var lk_label = {
				1:"I",
				2:"II",
				3:"III",
				4:"IV",
				5:"V",
				6:"VI",
				7:"VII",
			};
			
			var text = "";
			if( (maxVal - minVal) == 1 ){
				text = lk_label[maxVal]+" only";
			}else if((maxVal - minVal) > 1){
				minVal++;
				text = lk_label[minVal]+"-"+lk_label[maxVal];
			}
			
			$(target+" .noUi-connect").attr("data-aftercontent", text);
			
		}
		
		noUiSlider.create(inlinetext, {
			connect: [false, true],
			step: 1,
			behaviour: 'tap',
			start: 5,
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#inlinetext", 5, 7);
		
		noUiSlider.create(indicator_only, {
			connect: true,
			step: 1,
			behaviour: 'tap',
			start: [ 4, 5 ],
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#indicator_only", 4,5);
		
		noUiSlider.create(noalert, {
			connect: [true, false],
			step: 1,
			behaviour: 'tap',
			start: 4 ,
			padding:1,
			range: {
				'min': 0,
				'max': 7
			},
		});
		renderLabel("#noalert", 0, 4);
		
		
		inlinetext.noUiSlider.on('change', function(){
			
			var val = inlinetext.noUiSlider.get();
			indicator_only.noUiSlider.set([null, val]);
			
			renderLabel("#inlinetext", val, 7);
			
			var vals = indicator_only.noUiSlider.get();
			console.log("CHECK vals:", vals);
			if(vals[0] > val ){
				vals[1] = vals[0] = val;
				indicator_only.noUiSlider.set([val, val]);
			}
			
			renderLabel("#indicator_only", vals[0], vals[1]);
			
			
		});
		
		indicator_only.noUiSlider.on('change', function(){
			var vals = indicator_only.noUiSlider.get();
			var startVal= vals[0];
			var endVal 	= vals[1];
			inlinetext.noUiSlider.set(endVal);
			
			renderLabel("#indicator_only", vals[0], vals[1]);
			renderLabel("#inlinetext", endVal, 7);
			
			noalert.noUiSlider.set(startVal);
			renderLabel("#noalert", 0, startVal);
			
		});
		
		indicator_only.noUiSlider.on('set', function(){
			var vals = indicator_only.noUiSlider.get();
			var startVal= vals[0];
			var endVal 	= vals[1];
			inlinetext.noUiSlider.set(endVal);
			
			renderLabel("#indicator_only", vals[0], vals[1]);
			renderLabel("#inlinetext", endVal, 7);
			
			noalert.noUiSlider.set(startVal);
			renderLabel("#noalert", 0, startVal);
			
		});
		
		noalert.noUiSlider.on('change', function(){
			var val = noalert.noUiSlider.get();
			
			renderLabel("#noalert", 0, val);
			
			indicator_only.noUiSlider.set(val);
			var vals = indicator_only.noUiSlider.get();
			if(vals[0] > vals[1] ){
				vals[1] = vals[0] = val;
				indicator_only.noUiSlider.set([val, val]);
			}
			renderLabel("#indicator_only", vals[0], vals[1]);
			
		});
		
	}
	
	render() {
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<SidebarSettings activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						<table className="table-panel table-panel-bordered">
							<thead>
								<tr>
									<th colSpan="6" ><p className="text-medium text-gray font-semibold">Interventions by Target Area of Care</p></th>
								</tr>
							</thead>
							<tbody className="text-muted">
								<tr>
									<td className="text-medium text-gray font-semibold">Preventive Care</td>
									<td className="text-medium text-gray font-semibold">Diagnosis</td>
									<td className="text-medium text-gray font-semibold">Treatment</td>
									<td className="text-medium text-gray font-semibold">Followup Management</td>
									<td className="text-medium text-gray font-semibold">Efficency & Costs</td>
									<td className="text-medium text-gray font-semibold">Patient Experience</td>
								</tr>
								<tr>
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett1" name="check" />
												  <label htmlFor="sett1"></label>
												</div>
												Genetic risks
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett2" name="check" />
												  <label htmlFor="sett2"></label>
												</div>
												Pop health outreach
											</div>
										</div>
									</td>
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett3" name="check" />
												  <label htmlFor="sett3"></label>
												</div>
												Test and lab recommendations
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett4" name="check" />
												  <label htmlFor="sett4"></label>
												</div>
												Genetic test findings
											</div>
										</div>
									</td>
									
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett5" name="check" defaultChecked />
												  <label htmlFor="sett5"></label>
												</div>
												First medication decisions
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett6" name="check" />
												  <label htmlFor="sett6"></label>
												</div>
												Medication reconciliation
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett7" name="check" />
												  <label htmlFor="sett7"></label>
												</div>
												Dosage & drug interractions
											</div>
										</div>
									</td>
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett8" name="check" />
												  <label htmlFor="sett8"></label>
												</div>
												Adverse event
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett9" name="check" />
												  <label htmlFor="sett9"></label>
												</div>
												Corollary orders
											</div>
										</div>
									</td>
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett10" name="check" />
												  <label htmlFor="sett10"></label>
												</div>
												Care plans
											</div>
										</div>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett11" name="check" />
												  <label htmlFor="sett11"></label>
												</div>
												Order sets
											</div>
										</div>
									</td>
									<td>
										<div className="list-item">
											<div className="text-medium text-gray font-semibold" >
												<div className="checkbox-primary">
												  <input type="checkbox" value="None" id="sett12" name="check" />
												  <label htmlFor="sett12"></label>
												</div>
												Duplicate testing alerts
											</div>
										</div>
									</td>
									
								</tr>
								
							</tbody>
						</table>
						
						<table id="settings-levels" className="table-panel table-panel-bordered">
							<thead>
								<tr>
									<th><p className="text-medium text-gray font-semibold">Alerts by Evidence Level</p></th>
								</tr>
							</thead>
							<tbody className="text-muted">
								<tr>
									<td className="text-medium text-gray font-semibold">
										<div className="m30-l">
											<p>Level I: Evidence from a systematic review of all relevant randomized controlled trials (RCT's), or evidence-based clinical practice guidelines based on systematic reviews of RTC\'s</p>
											<p>Level II: Evidence obtained from at least one well-designed Randomized Controlled Trial (RTC)</p>
											<p>Level III: Evidence obtained from well-designed controlled trials without randomization, quasi -experimental</p>
											<p>Level IV: Evidence from well-designed case-control and cohort studies</p>
											<p>Level V: Evidence from systematic reviews of descriptive and qualitative studies</p>
											<p>Level VI: Evidence from a single descriptive or qualitative study</p>
											<p>Level VII: Evidence from the opinion of authorities and/or reports of expert committees</p>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										
										
										<div className="row m-b">
											<div className="col-md-2">
												<p className="text-medium text-blue font-semibold">Interruptive Alerts</p>
											</div>
											<div className="col-md-10">
												<p className="text-medium text-blue font-semibold text-center">Evidence Level</p>
											</div>
										</div>
										<div className="row multirange rangeend">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">Hard stop</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="hardstop"></div>
											</div>	
										</div>
										<div className="row multirange">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">Required override reason</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="required_override_reason"></div>
											</div>
										</div>
										
										<div className="row multirange rangestart">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">Optional override reason</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="optional_override_reason"></div>
											</div>	
										</div>
										
										
									</td>
								</tr>
								
								<tr>
									<td>
										
										
										<div className="row m-b">
											<div className="col-md-2">
												<p className="text-medium text-blue font-semibold">Passive Alerts</p>
											</div>
										</div>
										<div className="row multirange rangeend">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">Inline text</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="inlinetext"></div>
											</div>	
										</div>
										<div className="row multirange">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">Indicator only</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="indicator_only"></div>
											</div>
										</div>
										
										<div className="row multirange rangestart">
											<div className="col-md-2">
												<label className="m-l text-medium text-gray font-semibold">No alert</label>
											</div>
											
											<div className="col-md-8 col-md-offset-1">
												<div id="noalert"></div>
											</div>	
										</div>
										
										
									</td>
								</tr>
								
							</tbody>
						</table>
						
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default Settings;
