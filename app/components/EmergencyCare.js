import React from 'react';

import Header from './Header';
import Footer from './Footer';

import Sidebar from './Sidebar';
import MedicalHistory from './sections/MedicalHistory';

class EmergencyCare extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = {
			activeMenu:"emergency_care",
			patient:{}
		};
	}
	
	componentDidMount(){
		
		var config = {
			container: "#common-conditions-tree",
			levelSeparation:    250,
			rootOrientation:  'WEST', // NORTH || EAST || WEST || SOUTH
			siblingSeparation:  70,
			nodeAlign: "BOTTOM",
			connectors: {
				//type: "step",
				style: {
					"stroke-width": 2,
					"stroke": "#2f4050",
					"stroke-dasharray": "_", //"", "-", ".", "-.", "-..", ". ", "- ", "--", "- .", "--.", "--.."
					"arrow-end": "classic-wide-long"
				}
			}
		};

		var parent_node = {
			//text: { name: "Patients Like Michael", desc: "Asian male 30-40 by nonspecific chest pain. Taking amitriptilyn corispodol." },
			innerHTML:'<p class="node-name">Patients Like Michael <br/><i class="fa fa-info-circle"></i></p> <p class="node-desc">Asian male 30-40 by nonspecific chest pain. Taking amitriptilyn corispodol.</p>',
			HTMLclass: "root-node"
		};

		var first_child = {
			parent: parent_node,
			//text: { name: "First child", desc:"50%" },
			innerHTML:'<p class="node-name">Atherosclerosis <i class="fa fa-info-circle"></i> <span class="btn btn-primary pull-right">See Order Set</span></p><p class="node-desc">50%</p>',
			HTMLclass: "childs"
		};

		var second_child = {
			parent: parent_node,
			//text: { name: "Second child", desc:"50%" },
			innerHTML:'<p class="node-name">Atrial Fibrillation <i class="fa fa-info-circle"></i> <span class="btn btn-primary pull-right">See Order Set</span></p><p class="node-desc">50%</p>',
			HTMLclass: "childs"
		};

		var third_child = {
			parent: parent_node,
			//text: { name: "Third child", desc:"50%" },
			innerHTML:'<p class="node-name">Hypertension <i class="fa fa-info-circle"></i> <span class="btn btn-primary pull-right">See Order Set</span></p><p class="node-desc">50%</p>',
			HTMLclass: "last-childs"
		};

		var simple_chart_config = [
			config, parent_node, first_child, second_child, third_child
		];
		
		
		new Treant( simple_chart_config );
		
		fetch('/patient', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				
				response.json().then((json) => {
					this.setState({ patient:json });
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	render() {
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<Sidebar activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						
						<div className="panel panel-default" >
							<div className="panel-heading">
								EMERGENCY DECISION SUPPORT
							</div>
							<div className="panel-body">
								
								<div className="row" >
									<div className="col-md-12 m15-b font-semibold text-small">
										COMMON CONDITIONS
									</div>
								</div>
								
								<div className="panel panel-default">
									<div className="panel-body">
										<div id="common-conditions-tree"></div>
									</div>
								</div>
								
								<div className="row" >
									<div className="col-md-12 m15-b font-semibold text-medium">
										MEDICATION INTERACTIONS
										<div className="pull-right">
											<span>Historical Medications OFF </span>
											<div className="switch pull-right" >
											  <input id="switch-toggle-1" className="switch-toggle switch-toggle-round" type="checkbox" />
											  <label htmlFor="switch-toggle-1"></label>
											</div>
										</div>
									</div>
								</div>
								
								<table id="medication-interactions" className="table-panel vertical-middle table-panel-bordered">
									<thead>
										<tr>
											<th><p className="text-medium text-gray font-semibold">Name</p></th>
											<th><p className="text-medium text-gray font-semibold">Possible Adverse Reactions</p></th>
											<th colSpan="2"><p className="text-medium text-gray font-semibold">Recommendations</p></th>
										</tr>
									</thead>
									<tbody className="text-muted">
										<tr>
											<td>
												<div>
													<p className="text-medium text-blue">Amitriptyline <i className="fa fa-info-circle"></i></p>
													<p className="text-muted">First diagnosed 08/15/2013</p>
												</div>
											</td>
											<td>
												<p className="text-medium text-blue">Poor Metabolizer</p>
											</td>
											<td>
												<p className="text-muted">Increased side effects</p>
											</td>
											<td>
												<span className="icon-status m15-b text-danger glyphicon glyphicon-remove-sign"></span>
												<button className="btn btn-primary m5-b" type="button" >Find Alternative</button>
												<button className="btn btn-primary" type="button" >Stop Medication</button>
											</td>
										</tr>
										
										<tr>
											<td>
												<div>
													<p className="text-medium text-blue">Carisoprodol <i className="fa fa-info-circle"></i></p>
													<p className="text-muted">First diagnosed 08/15/2013</p>
													<p className="text-muted">Ordered by Kazi Doug MD</p>
												</div>
											</td>
											<td>
												<p className="text-medium text-blue">Poor Metabolizer</p>
											</td>
											<td>
												<p className="text-muted">Increased side effects</p>
											</td>
											<td>
												<span className="icon-status m15-b text-warning glyphicon glyphicon-exclamation-sign"></span>
												<button className="btn btn-primary m5-b" type="button" >Find Alternative</button>
												<button className="btn btn-primary" type="button" >Stop Medication</button>
											</td>
										</tr>
										
										<tr>
											<td>
												<div>
													<p className="text-medium text-blue">Multivitamin <i className="fa fa-info-circle"></i></p>
													<p className="text-muted">Started 07/15/2010</p>
												</div>
											</td>
											<td>
												<p className="text-medium text-blue">No Known PGx or Drug Interactions</p>
											</td>
											<td>
												<p className="text-muted">None</p>
											</td>
											<td>
												<span className="icon-status m15-b text-success glyphicon glyphicon-ok-sign"></span>
												<button className="btn btn-default m5-b" type="button" >Find Alternative</button>
												<button className="btn btn-default" type="button" >Stop Medication</button>
											</td>
										</tr>
										
									</tbody>
								</table>
								
								
							</div>
						</div>
						
						<MedicalHistory data = {this.state.patient.medical_history} />
						
						<div className="button-row text-center">
							<span className="btn btn-primary">LOAD MORE</span>
						</div>
					
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default EmergencyCare;
