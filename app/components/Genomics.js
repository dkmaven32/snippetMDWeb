import React from 'react';

import Header from './Header';
import Footer from './Footer';

import Sidebar from './Sidebar';

class Genomics extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"genomics",
		};
	}
	
	componentDidMount(){
			
		fetch('/patientlist', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				
				response.json().then((json) => {
					// this.setState({ patientlist:json.map((patient) =>
										// <tr>
											// <td>{patient.id}</td>
											// <td>{patient.patient}</td>
											// <td>{patient.reason_visit}</td>
											// <td>{patient.existing_conditions}</td>
											// <td>{patient.medications}</td>
											// <td>{patient.population_genetics}</td>
										// </tr>
									// )
					// });
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	render() {
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<Sidebar activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						
						
						<div className="row">
							<div className="col-md-12">
								<h4 className="title-section pull-left">GENETIC INFORMATION</h4>
							</div>
						</div>
						<div className="row">
							<div className="col-md-12 m15-b">
								<span className="text-muted text-blue m15-r" ><img className="icon-inline" src="images/tree-chart.png" />Organize by relation</span>
								<span className="text-muted text-red m15-r" ><img className="icon-inline" src="images/scale-desc.png" />Sort by risk level</span>
							</div>
						</div>
						
						<table className="table-normal no-border align-left text-muted table-panel-4x" >
							<thead>
								<tr className="border-b-solid" >
									<th>Potential Genetic Risks</th>
									<th>Confidence</th>
									<th>Patient's Risk</th>
									<th>Average Risk</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td className="text-medium text-blue">Coronary Heart Disease</td>
									<td><i className="circle-yellow-x4"></i></td>
									<td>39-56%</td>
									<td>46.8%</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>of Asian ethnicity who share Michael Lui's genotype are estimated to develop Coronary Heart Disease between the age of 35 and 65. </td>
									<td>of Asian ethnicity are estimated to develop Coronary Heart Disease between the age of 35 and 65 </td>
								</tr>
								<tr>
									<td></td>
									<td colSpan="3">
										<div className="m15-t"></div>
										<h3 className="text-medium text-gray">Factors used to estimate risk</h3>
										<div className="m15-t"></div>
										
										<table className="table table-bordered table-fluent-td">
											<tbody>
												<tr>
													<th className="font-semibold">Age</th>
													<th className="font-semibold">Ancestry</th>
													<th className="font-semibold">Sex</th>
													<th className="font-semibold">SNP</th>
												</tr>
												<tr>
													<td>35 - 65</td>
													<td>Asian</td>
													<td>Male</td>
													<td>
														rs10757278 (9p21 region), rs12526453 (PHACTR1), <br/>
														rs17468148 (CXCL12), rs1122608 (SMARCA4), rs12526453 (PHACTR1), <br/>
														rs17468148 (CXCL12), rs1122608 (SMARCA4), rs12526453 (PHACTR1), <br/>
														rs17468148 (CXCL12)
													</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
								<tr className="border-t-dashed">
									<td className="text-medium text-blue">Coronary Heart Disease</td>
									<td><i className="circle-yellow-x4"></i></td>
									<td>39-56%</td>
									<td>46.8%</td>
								</tr>
								<tr className="border-t-dashed">
									<td className="text-medium text-blue">Coronary Heart Disease</td>
									<td><i className="circle-yellow-x4"></i></td>
									<td>39-56%</td>
									<td>46.8%</td>
								</tr>
								<tr className="border-t-dashed">
									<td className="text-medium text-blue">Coronary Heart Disease</td>
									<td><i className="circle-yellow-x4"></i></td>
									<td>39-56%</td>
									<td>46.8%</td>
								</tr>
							</tbody>
						</table>
						
					
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default Genomics;
