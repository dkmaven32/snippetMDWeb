import React from 'react';

import Header from './Header';
import Footer from './Footer';

import SidebarMyPatients from './SidebarMyPatients';

class MyPatientSchedule extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"mypatientschedule",
			patientschedule:""
		};
	}
	
	componentDidMount(){

		$(".filter-date-schedule").datepicker({
			autoclose: true,
			format: "MM dd, yyyy",
			startDate: new Date(new Date().setFullYear(new Date().getFullYear() -1)),
			endDate: new Date(new Date().setFullYear(new Date().getFullYear() +1)),
		});
		
		fetch('/patientschedule', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				response.json().then((json) => {
					this.setState({ patientschedule:json.map((patient) =>
							  <tr>
								<td className="t-line" >
									<span className="vertical-text">{patient.agenda.time}</span> 
									<div className="circle"><img src="images/aspirin.png" className="mCS_img_loaded"/></div>
									<hr className="vtree-line"/>
								</td>
								<td className="text-medium text-blue font-semibold" ><div className="cell-cont">{patient.patient}</div></td>
								<td><div className="cell-cont">{patient.reason_visit}</div></td>
								<td><div className="cell-cont">{patient.existing_conditions}Good fit</div></td>
								<td><div className="cell-cont">{patient.medications}Uj dhe ajer</div></td>
								<td><div className="cell-cont">{patient.population_genetics}</div></td>
							</tr>
						) 
					});
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	render() {
		
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<SidebarMyPatients activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						
						<div className="row">
							<div className="col-md-12">
								<h4 className="title-section pull-left">MY PATIENTS</h4>
							</div>
						</div>
						<div className="row">
							<div className="col-md-6">
								<h4 className="title-section pull-left">Schedule for Today</h4>
							</div>
							<div className="col-md-6">
								
								<h4 className="title-section pull-right">
									<div className="input-group date cameleon-calendar filter-date-schedule">
										<input className="form-control" type="text" />
										<span className="input-group-addon icon"><i className="fa fa-calendar"></i></span>
									</div>
								</h4>
								
							</div>
						</div>
						
						<table className="table-normal vertical-middle table-dashed-bordered text-muted" >
							<thead>
								<tr>
									<th>Agenda</th>
									<th>Patient</th>
									<th>Reason for Visit</th>
									<th>Existing Conditions</th>
									<th>Medications</th>
									<th>Population Health & Genetic Risks</th>
								</tr>
							</thead>
							<tbody>
								{this.state.patientschedule}
							</tbody>
						</table>
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default MyPatientSchedule;
