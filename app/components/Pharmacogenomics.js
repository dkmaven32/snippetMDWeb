import React from 'react';

import Header from './Header';
import Footer from './Footer';

import Sidebar from './Sidebar';

class Pharmacogenomics extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"pharmacogenomics",
			SupportingData:{
				isVisible: true
			},
			Genomics:{
				isVisible: true 
			},
			Interactions:{
				isVisible: true 
			},
			PopulationHealth:{
				isVisible: false 
			},
			History:{
				isVisible: false 
			},
		};
	}
	
	componentDidMount(){
			
		fetch('/patientlist', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				
				response.json().then((json) => {
					// this.setState({ patientlist:json.map((patient) =>
										// <tr>
											// <td>{patient.id}</td>
											// <td>{patient.patient}</td>
											// <td>{patient.reason_visit}</td>
											// <td>{patient.existing_conditions}</td>
											// <td>{patient.medications}</td>
											// <td>{patient.population_genetics}</td>
										// </tr>
									// )
					// });
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	toggle(event) {
		var obj = {};
		obj[event.target.getAttribute("data-section")] = {isVisible: !this.state[event.target.getAttribute("data-section")].isVisible}; 
		this.setState( obj );
	}
	
	render() {
		
		var primaryCls = 'btn btn-primary';
		var defaultCls = 'btn btn-default';
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<Sidebar activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						
						<table id="successful-treatments" className="table-panel vertical-middle table-panel-bordered">
							<thead>
								<tr>
									<th colSpan="5" >MOST SUCCESSFUL TREATMENTS</th>
								</tr>
							</thead>
							<tbody className="text-muted">
								<tr>
									<td><p className="text-medium text-gray font-semibold">Conditions</p></td>
									<td><p className="text-medium text-gray font-semibold">Status</p></td>
									<td><p className="text-medium text-gray font-semibold">Current Treatment</p></td>
									<td><p className="text-medium text-gray font-semibold">Recommended Treatment</p></td>
									<td></td>
								</tr>
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Hypertension <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">First diagnosed 08/15/2013</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">Progression</p>
									</td>
									<td>
										<p className="text-medium text-blue">Daily aspirin <i className="fa fa-info-circle pull-right"></i></p>
										<p className="text-muted">First prescribed 08/15/2013</p>
									</td>
									<td>
										<p className="text-medium text-blue">Warfarin <i className="fa fa-info-circle pull-right"></i></p>
									</td>
									<td>
										<span className="btn btn-primary">View Order Set</span>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Atrial Fibrillation <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">First diagnosed 08/15/2013</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">Partial Response</p>
									</td>
									<td>
										<p className="text-medium text-blue">Apixaban <i className="fa fa-info-circle pull-right"></i></p>
										<p className="text-muted">First prescribed 08/15/2013</p>
									</td>
									<td>
										<p className="text-medium text-blue">Atorvastatin <i className="fa fa-info-circle pull-right"></i></p>
									</td>
									<td>
										<span className="btn btn-primary">View Order Set</span>
									</td>
								</tr>
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Hypoglycemia <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">First diagnosed 08/15/2013</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">Partial Response</p>
									</td>
									<td>
										<p className="text-medium text-blue">Diet <i className="fa fa-info-circle pull-right"></i></p>
									</td>
									<td>
										<p className="text-medium text-blue">Diet <i className="fa fa-info-circle pull-right"></i></p>
									</td>
									<td>
										<span className="btn btn-primary">View Order Set</span>
									</td>
								</tr>
							</tbody>
						</table>
						
						<table id="medication-interactions" className="table-panel vertical-middle table-panel-bordered">
							<thead>
								<tr>
									<th colSpan="5" >MEDICATION INTERACTIONS
										<div className="pull-right">
											<span>Historical Medications OFF </span>
											<div className="switch pull-right" >
											  <input id="switch-toggle-1" className="switch-toggle switch-toggle-round" type="checkbox"/>
											  <label htmlFor="switch-toggle-1"></label>
											</div>
										</div>
									</th>
								</tr>
							</thead>
							<tbody className="text-muted">
								<tr>
									<td><p className="text-medium text-gray font-semibold">Name</p></td>
									<td><p className="text-medium text-gray font-semibold">Phenotype</p></td>
									<td><p className="text-medium text-gray font-semibold">Implications</p></td>
									<td><p className="text-medium text-gray font-semibold">Recommendations</p></td>
									<td></td>
								</tr>
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Amitriptyline <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">Started 08/15/2016</p>
											<p className="text-muted">Ordered by Kazi Doug MD</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">Poor Metabolizer</p>
									</td>
									<td>
										<p className="text-muted">Increased side effects</p>
									</td>
									<td>
										<p className="text-muted">Avoid tricyclic use due to potential for side effects. Consider alternative drug or 50% reduction of recommended starting dose</p>
									</td>
									<td>
										<span className="icon-status m15-b text-danger glyphicon glyphicon-remove-sign"></span>
										<button className="btn btn-primary m5-b" type="button" >Find Alternative</button>
										<button className="btn btn-primary" type="button" >Stop Medication</button>
									</td>
								</tr>
								
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Carisoprodol <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">Started 10/20/2016</p>
											<p className="text-muted">Ordered by Kazi Doug MD</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">Moderate Interaction with Amitriptyline</p>
									</td>
									<td>
										<p className="text-muted">Dizziness, drowsiness, confusion, and difficulty concentrating.</p>
									</td>
									<td>
										<p className="text-muted">Consider alternative to amitriptyline such as droxapin, trimpramine, or duloxetine.</p>
									</td>
									<td>
										<span className="icon-status m15-b text-warning glyphicon glyphicon-exclamation-sign"></span>
										<button className="btn btn-primary m5-b" type="button" >Find Alternative</button>
										<button className="btn btn-primary" type="button" >Stop Medication</button>
									</td>
								</tr>
								
								<tr>
									<td>
										<div>
											<p className="text-medium text-blue">Multivitamin <i className="fa fa-info-circle"></i></p>
											<p className="text-muted">Started 07/15/2010</p>
										</div>
									</td>
									<td>
										<p className="text-medium text-blue">No Known PGx or Drug Interactions</p>
									</td>
									<td>
										<p className="text-muted">None</p>
									</td>
									<td>
										<p className="text-muted">None</p>
									</td>
									<td>
										<span className="icon-status m15-b text-success glyphicon glyphicon-ok-sign"></span>
										<button className="btn btn-default m5-b" type="button" >Find Alternative</button>
										<button className="btn btn-default" type="button" >Stop Medication</button>
									</td>
								</tr>
								
							</tbody>
						</table>
						
						<div className="accordion accordion-default" data-section="SupportingData" onClick={this.toggle.bind(this)}>
							SUPPORTING DATA
							<i className={this.state.SupportingData.isVisible ? 'fa fa-chevron-up pull-right' : 'fa fa-chevron-down pull-right' } ></i>
						</div>
						
						<div id="supporting-data" className={this.state.SupportingData.isVisible ? "accordion-container fadein-show" : "accordion-container fadein-hide" } >
						
							<div className="button-row">
								<span className="btn btn-primary" className={this.state.Genomics.isVisible ? primaryCls : defaultCls} data-section="Genomics" onClick={this.toggle.bind(this)} >Genomics</span>
								<span className="btn btn-primary" className={this.state.Interactions.isVisible ? primaryCls : defaultCls} data-section="Interactions" onClick={this.toggle.bind(this)}>Interactions</span>
								<span className="btn btn-default" className={this.state.PopulationHealth.isVisible ? primaryCls : defaultCls} data-section="PopulationHealth" onClick={this.toggle.bind(this)}>Population Health</span>
								<span className="btn btn-default" className={this.state.History.isVisible ? primaryCls : defaultCls} data-section="History" onClick={this.toggle.bind(this)} >History</span>
								
								<span className="pull-right text-small">Save preset <input type="checkbox" name="preset" value="1"/></span>
							</div>
							
							<section className={this.state.Genomics.isVisible ? "fadein-show" : "fadein-hide" } >
								<table id="genomics" className="table-panel vertical-middle table-panel-bordered fadein-show" >
									<thead>
										<tr>
											<th><span className="m-r">Genomics</span> <i className="fa fa-arrows"></i></th>
											<th colSpan="2" >
												<span className="m30-r text-italic text-muted font-regular"><i className="circle-yellow-x1"></i>Low confidence</span>
												<span className="m30-r text-italic text-muted font-regular">
													<i className="circle-yellow-x5"></i>
													High confidence
												</span>
											</th>
										</tr>
									</thead>
									<tbody className="text-muted">
										<tr>
											<td>
												<div>
													<p className="text-medium font-semibold">Gene: CYP2C19</p>
													<p className="text-medium font-semibold">Genotype: *2&#47;*2</p>
												</div>
											</td>
											<td>
												<p className="text-medium font-semibold">Phenotype: Poor Metabolizer of CYP2C19</p>
												<p className="text-medium font-semibold">Impacts Amitriptyline. Adjust medication or dose.</p>
											</td>
											<td>
												<span className="m-r">Confidence:</span> <i className="circle-yellow-x4 m-r"></i> <i className="fa fa-info-circle"></i>
											</td>
										</tr>
										
										<tr>
											<td>
												<div>
													<p className="text-medium font-semibold">Gene: CYP2B6</p>
													<p className="text-medium font-semibold">Genotype: *5&#47;*5</p>
												</div>
											</td>
											<td>
												<p className="text-medium font-semibold">Phenotype: Poor Metabolizer of CYP2B6</p>
												<p className="text-medium font-semibold">Impacts Amitriptyline. Adjust medication or dose.</p>
											</td>
											<td>
												<span className="m-r">Confidence:</span> <i className="circle-yellow-x4 m-r"></i> <i className="fa fa-info-circle"></i>
											</td>
										</tr>
										
										<tr>
											<td>
												<div>
													<p className="text-medium font-semibold">Gene: CYP2C9</p>
													<p className="text-medium font-semibold">Genotype: *2&#47;*2</p>
												</div>
											</td>
											<td>
												<p className="text-medium font-semibold">Phenotype: Poor Metabolizer of CYP2C9</p>
												<p className="text-medium font-semibold">Impacts Amitriptyline. Adjust medication or dose.</p>
											</td>
											<td>
												<span className="m-r">Confidence:</span> <i className="circle-yellow-x4 m-r"></i> <i className="fa fa-info-circle"></i>
											</td>
										</tr>
										
										
										
									</tbody>
								</table>
							</section>
							
							<section className={this.state.Interactions.isVisible ? "fadein-show" : "fadein-hide" } >
								<table id="interactions" className="table-panel vertical-middle table-panel-bordered fadein-show" >
									<thead>
										<tr>
											<th>Interactions</th>
											<th colSpan="2" >Implications and Recommendations</th>
										</tr>
									</thead>
									<tbody className="text-muted">
										<tr>
											<td>
												<div>
													<p className="text-medium text-blue font-semibold">Amitriptyline - Carisoprodol</p>
													<p className="text-muted">Interaction: Moderate</p>
												</div>
											</td>
											<td>
												<p className="text-muted">
													MONITOR: Central nervous system- and/or respiratory-depressant effects may be additively or synergistically increased in patients taking multiple drugs that cause these effects, especially in elderly or debilitated patients.
													<br/><br/>MANAGEMENT: During concomitant use of these drugs, patients should be monitored for potentially excessive or prolonged CNS and respiratory depression. AMbulatory patients should be counseled to avoid hazardous activities requiring mental alertness and motor coordination until they know how these agents affect them, and to notify their physician if they experience excessive or prolonged CNS effects that interfere with their normal activities.
												</p>
											</td>
											<td>
												<span className="icon-status m15-b text-warning glyphicon glyphicon-exclamation-sign"></span>
												<button className="btn btn-primary m5-b" type="button" >Find Alternative</button>
												<button className="btn btn-primary" type="button" >Stop Medication</button>
											</td>
										</tr>
										
									</tbody>
								</table>
							</section>
							
						</div>
						
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default Pharmacogenomics;
