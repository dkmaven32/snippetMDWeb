import React from 'react';

import Header from './Header';
import Footer from './Footer';

import Sidebar from './Sidebar';
import MedicalJournal from './sections/MedicalJournal';
import SupportingData from './sections/SupportingData';
import MedicalHistory from './sections/MedicalHistory';

class PatientProfile extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"patientprofile",
			patient:{}
		};
	}
	
	componentDidMount(){
			
		fetch('/patient', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				
				response.json().then((json) => {
					console.log(json);
					this.setState({ patient:json });
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	render() {
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<Sidebar activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						<MedicalJournal data = {this.state.patient.medical_journal} />
						
						<SupportingData data = {this.state.patient.supporting_data} />
						
						<MedicalHistory data = {this.state.patient.medical_history} />
						
						<div className="button-row text-center">
							<span className="btn btn-primary">LOAD MORE</span>
						</div>
						
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default PatientProfile;
