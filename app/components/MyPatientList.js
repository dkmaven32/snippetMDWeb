import React from 'react';

import Header from './Header';
import Footer from './Footer';

import SidebarMyPatients from './SidebarMyPatients';

class MyPatientList extends React.Component {
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"mypatientlist",
			patientlist:""
		};
	}
	
	componentDidMount(){
			
		fetch('/patientlist', {
		  method: 'get',
		  headers: { 'Content-Type': 'application/json' },
		  // body: JSON.stringify({
			// name: name,
			// email: email,
			// message: message
		  // })
		}).then((response) => {
			if(response.ok){
				
				response.json().then((json) => {
					this.setState({ patientlist:json.map((patient) =>
										<tr>
											<td>{patient.id}</td>
											<td>{patient.patient}</td>
											<td>{patient.reason_visit}</td>
											<td>{patient.existing_conditions}</td>
											<td>{patient.medications}</td>
											<td>{patient.population_genetics}</td>
										</tr>
									)
					});
				});
			}else{
				console.log("RESPONSE in ERROR");
			}
		});
		
	}
	
	render() {
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<SidebarMyPatients activeMenu={this.state.activeMenu}/>
				<div id="content" className="colbox">
					<div className="content-box">
						
						<div className="row">
							<div className="col-md-12">
								<h4 className="title-section pull-left">MY PATIENTS</h4>
							</div>
						</div>
						<div className="row">
							<div className="col-md-6">
								<h4 className="title-section pull-left">All Patients</h4>
							</div>
							<div className="col-md-6">
								
							</div>
						</div>
						
						<table className="table-normal vertical-middle table-dashed-bordered text-muted" >
							<thead>
								<tr>
									<th>ID</th>
									<th>Patient</th>
									<th>Last visit & Reason</th>
									<th>Existing Conditions</th>
									<th>Medications</th>
									<th>Population Health & Genetic Risks</th>
								</tr>
							</thead>
							<tbody>
								{this.state.patientlist}
							</tbody>
						</table>
					
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default MyPatientList;
