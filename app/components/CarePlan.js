import React from 'react';
import Header from './Header';
import Footer from './Footer';

import Sidebar from './Sidebar';

class CarePlan extends React.Component {
	
	componentDidMount(){
			
		//console.log("totWidth", $("#careplan-calendar").width() - 244 );
		
		$(".care-calendar-container").width( $("#careplan-calendar").width() - 244 );
		
		$(".care-calendar-container").mCustomScrollbar({
			axis:"x",
			theme:"dark",
			scrollbarPosition:"outside"
		});
		
	}
  
	constructor(props) {
		super(props);
		this.state = { 
			activeMenu:"careplan",
		};
	}
	
	toggle(event) {
		
		var el = event.target;
		if(el.nodeName.toLowerCase() == 'i'){
			el = event.target.parentElement;
		}
		if(el.getAttribute("data-type") == 'root' ){
			if($(".rows-"+el.getAttribute("data-block")).hasClass('root-hidden')){
				$(".rows-"+el.getAttribute("data-block")).removeClass('root-hidden');
				
				//Switching icon:
				$(".icon-"+el.getAttribute("data-block")+" i").removeClass("fa-caret-down");
				$(".icon-"+el.getAttribute("data-block")+" i").addClass("fa-caret-up");
			}else{ 
				$(".rows-"+el.getAttribute("data-block")).addClass('root-hidden');
				
				//Switching icon:
				$(".icon-"+el.getAttribute("data-block")+" i").removeClass("fa-caret-up");
				$(".icon-"+el.getAttribute("data-block")+" i").addClass("fa-caret-down");
			}
		}else{
			if($(".rows-"+el.getAttribute("data-root")+".rows-"+el.getAttribute("data-block")).hasClass('parent-hidden')){
				$(".rows-"+el.getAttribute("data-root")+".rows-"+el.getAttribute("data-block")).removeClass('parent-hidden');
				
				//Switching icon:
				$(".icon-"+el.getAttribute("data-block")+" i").removeClass("fa-caret-down");
				$(".icon-"+el.getAttribute("data-block")+" i").addClass("fa-caret-up");
				
			}else{ 
				$(".rows-"+el.getAttribute("data-root")+".rows-"+el.getAttribute("data-block")).addClass('parent-hidden');
				
				//Switching icon:
				$(".icon-"+el.getAttribute("data-block")+" i").removeClass("fa-caret-up");
				$(".icon-"+el.getAttribute("data-block")+" i").addClass("fa-caret-down");
			}
		}
	}
   
	render() {
		
		var collapsedCls = "text-muted hidden";
		var relapsedCls = "text-muted";
		
		var triangleDown = "fa fa-caret-down text";
		var triangleUp = "fa fa-caret-up text";
		
		return (
		  <div>
			<Header/>
			<section className="hbox content-wrapper">
				
				<Sidebar activeMenu={this.state.activeMenu} />
				<div id="content" className="colbox">
					<div className="content-box">
						<div className="row">
							<div className="col-md-12">
								<h4 className="title-section pull-left">SUMMARY</h4>
							</div>
						</div>
						<div className="row">
							<div className="col-md-12">
								<p className="text-muted">
									Based on the best outcomes of patients like Mike in our population, Mike should focus on these three things:
								</p>
								<dl className="dl-horizontal m-b">
									<dt>a)</dt>
									<dd className="text-muted" >Medication adherence: 88% of similar patients report successful outcomes in managing their conditions.</dd>
									<dt>b)</dt>
									<dd className="text-muted" >Avoiding citrus fruits such as grapefruit that block medication uptake to the bloodstream. Mike’s genes make him especially susceptible to this.</dd>
									<dt>c)</dt>
									<dd className="text-muted" >Watch for signs of prolonged chest or general discomfort, potential early indicators of disease. Mike’s family history suggests this will be a risk.</dd>
								</dl>
							</div>
						</div>
						
						<div className="row">
							<div className="col-md-12">
								<h4 className="title-section pull-left">TREATMENT & MILESTONES</h4>
							</div>
						</div>
						<div className="row">
							<div className="col-md-7">
								<span className="text-muted label font-regular m-r">Filter by</span>
								<input type="text" className="form-control inline-input input-sm m-r" placeholder="Condition" />
								<input type="text" className="form-control inline-input input-sm m-r" placeholder="Goal" />
								<button className="btn btn-primary input-min">Apply</button>
							</div>
							<div className="col-md-5">
								<span className="text-muted label font-regular">Jump to date</span>
								<div className="date-group-form">
									<input type="text" className="form-control day-input" placeholder="DD" />
									<input type="text" className="form-control month-input" placeholder="MM" />
									<input type="text" className="form-control year-input" placeholder="YYYY" />
								</div>
								<button className="btn btn-primary inline-input input-min">Go</button>
							</div>
						</div>
						
						<div className="row">
							<div className="col-md-12">
							
								<div id="careplan-calendar">
									<div className="care-activities">
										<div className="care-row care-options">
											
											<div className="col-md-4">
												<span className="text-muted label font-regular">Monthly</span>
											</div>
											<div className="col-md-4">
												<div className="switch">
												  <input id="switch-toggle-1" className="switch-toggle switch-toggle-round" type="checkbox" />
												  <label htmlFor="switch-toggle-1"></label>
												</div>
											</div>
											<div className="col-md-4">
												<span className="text-muted label font-regular">Daily</span>
											</div>
											<div className="clear"></div>
										</div>
										<div className="care-row care-activity-primary">
											<p className="text-medium text-blue font-semibold">
												Clinical Treatments 
												<span className="a-pointer rowcollapser pull-right icon-root-1"  onClick={this.toggle.bind(this)} data-type="root" data-block="root-1">
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity-secondary rows-root-1"  >
											<p className="text-medium text-blue font-semibold">Medications <img src="images/plus_circle.png" className="m15-l" /> 
												<span className="a-pointer rowcollapser pull-right icon-parent-1" onClick={this.toggle.bind(this)} data-root="root-1" data-block="parent-1" >
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity rows-root-1 rows-parent-1 text-muted">
											Lipitor
										</div>
										<div className="care-row care-activity rows-root-1 rows-parent-1 text-muted">
											Naproxin
										</div>
										
										<div className="care-row care-activity-secondary rows-root-1"  >
											<p className="text-medium text-blue font-semibold">Lab tests <img src="images/plus_circle.png" className="m15-l" /> 
												<span className="a-pointer rowcollapser pull-right icon-parent-2" onClick={this.toggle.bind(this)} data-root="root-1" data-block="parent-2" >
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity rows-root-1 rows-parent-2 text-muted">
											CT Scan
										</div>
										<div className="care-row care-activity rows-root-1 rows-parent-2 text-muted">
											Cholesterol
										</div>
										
										<div className="care-row care-activity-primary">
											<p className="text-medium text-blue font-semibold">
												Non-Clinical Treatments 
												<span className="a-pointer rowcollapser pull-right icon-root-2"  onClick={this.toggle.bind(this)} data-type="root" data-block="root-2">
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity-secondary rows-root-2"  >
											<p className="text-medium text-blue font-semibold">Physical activity <img src="images/plus_circle.png" className="m15-l" /> 
												<span className="a-pointer rowcollapser pull-right icon-parent-1" onClick={this.toggle.bind(this)} data-root="root-2" data-block="parent-1" >
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity rows-root-2 rows-parent-1 text-muted">
											Lorem Ipsum
										</div>
										
										<div className="care-row care-activity-secondary rows-root-2"  >
											<p className="text-medium text-blue font-semibold">Nutrition <img src="images/plus_circle.png" className="m15-l" /> 
												<span className="a-pointer rowcollapser pull-right icon-parent-2" onClick={this.toggle.bind(this)} data-root="root-2" data-block="parent-2" >
													<i className="fa fa-caret-up"></i>
												</span>
											</p>
										</div>
										<div className="care-row care-activity rows-root-2 rows-parent-2 text-muted">
											Lorem Ipsum
										</div>
										
										
									</div>
									
									<div className="care-calendar-container">
									
										<table id="cal-scroller-container" cellspacing="0" cellpadding="0">
											<tbody>
												<tr>
													<td>
														<div className="year-calendar text-muted">
															<div className="calendar-header">
																<div className="year-cell">
																	<div className="cell-container">2016</div>
																</div>
																<div className="months-row">
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">F</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">M</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">A</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">M</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">A</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">S</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">O</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">N</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">D</div>
																	</div>
																	<div className="clearfix"></div>
																</div>
															</div>
															<div className="cells-container">
																<div className="row-primary">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-secondary rows-root-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-1 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-cells rows-root-1 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-secondary rows-root-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-1 rows-parent-2">
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-cells rows-root-1 rows-parent-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-primary">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-secondary rows-root-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-2 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/run.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-secondary rows-root-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-2 rows-parent-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																
															</div>
														</div>
														
													</td>
													<td>													
														<div className="year-calendar text-muted">
															<div className="calendar-header">
																<div className="year-cell">
																	<div className="cell-container">2017</div>
																</div>
																<div className="months-row">
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">F</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">M</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">A</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">M</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">J</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">A</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">S</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">O</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">N</div>
																	</div>
																	<div className="cell">
																		<div className="cell-container">D</div>
																	</div>
																	<div className="clearfix"></div>
																</div>
															</div>
															<div className="cells-container">
																<div className="row-primary">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-secondary rows-root-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-1 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-cells rows-root-1 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/aspirin.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-secondary rows-root-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-1 rows-parent-2">
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-cells rows-root-1 rows-parent-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/chemistry.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-primary">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																<div className="row-secondary rows-root-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-2 rows-parent-1">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/run.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
																
																
																<div className="row-secondary rows-root-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																<div className="row-cells rows-root-2 rows-parent-2">
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell">
																		<div className="circle">
																			<img src="images/nutrition.png" />
																		</div>
																	</div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="cell"></div>
																	<div className="clearfix"></div>
																</div>
																
															</div>
														</div>
														
													</td>
												</tr>
											</tbody>
										</table>
										
									</div>
									
									<div className="clear"></div>
									
								</div>

							</div>
						</div>
						
					</div>
				</div>
				
			</section>
		  </div>
		);
	}
}

export default CarePlan;
