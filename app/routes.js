import React from 'react';
import { IndexRoute, Route } from 'react-router';
import App from './components/App';
import PatientProfile from './components/PatientProfile';
import CarePlan from './components/CarePlan';
import MyPatientSchedule from './components/MyPatientSchedule';
import MyPatientList from './components/MyPatientList';
import Pharmacogenomics from './components/Pharmacogenomics';
import Genomics from './components/Genomics';
import EmergencyCare from './components/EmergencyCare';
import Settings from './components/Settings';
import NotFound from './components/NotFound';

export default function getRoutes(store) {
  const clearMessages = () => {
    store.dispatch({
      type: 'CLEAR_MESSAGES'
    });
  };
  return (
    <Route path="/" component={App}>
      <IndexRoute component={PatientProfile} onLeave={clearMessages}/>
      <Route path="/patientprofile" component={PatientProfile} onLeave={clearMessages}/>
      <Route path="/careplan" component={CarePlan} onLeave={clearMessages}/>
      <Route path="/mypatientschedule" component={MyPatientSchedule} onLeave={clearMessages}/>
      <Route path="/mypatientlist" component={MyPatientList} onLeave={clearMessages}/>
      <Route path="/pharmacogenomics" component={Pharmacogenomics} onLeave={clearMessages}/>
      <Route path="/genomics" component={Genomics} onLeave={clearMessages}/>
      <Route path="/emergency_care" component={EmergencyCare} onLeave={clearMessages}/>
      <Route path="/settings" component={Settings} onLeave={clearMessages}/>
      <Route path="*" component={NotFound} onLeave={clearMessages}/>
    </Route>
  );
}
