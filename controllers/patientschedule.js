/**
 * GET /contact
 */
exports.patientscheduleGet = function(req, res) {
  // res.render('patient', {
    // title: 'Patient',
	// name:"Tedi",
	// lastname:"Cela"
  // });
  
  res.status(200).send([
	{
		agenda:{
			time:"8.30am",
			type:"checkup"
		},
		patient:"Tedi Cela",
		reason_visit:"Annual checkup",
		existing_conditions:"good fit",
		medications:"nothing",
		population_genetics:"no risk 0%"
	},
	{
		agenda:{
			time:"8.30am",
			type:"examination"
		},
		patient:"Jason Brian",
		reason_visit:"feeling powerless continuosly",
		existing_conditions:"low sugar",
		medications:"insuline",
		population_genetics:"diabet 10%"
	},
  ]);
  
};

