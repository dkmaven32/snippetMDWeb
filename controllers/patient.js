/**
 * GET /contact
 */
exports.patientGet = function(req, res) {
  // res.render('patient', {
    // title: 'Patient',
	// name:"Tedi",
	// lastname:"Cela"
  // });
  
  res.status(200).send({
		medical_journal:{
			conditions: {
				conditions:{
				
				},
				patient_goals:{
				
				},
				treatment_summary:{
				
				},
				risks:{
				
				}
			}
		},
		supporting_data:{
			genetics:{
				patient_risk:{
					percentage:"39-56%",
					numeric:"From 39 to 65 out 100 men",
					description:"of Asian ethnicity who share Michael Lui's genotype are estimated to develop Coronary Heart Disease between the age of 35 and 65."
				},
				average_risk:{
					percentage:"25.8%",
					numeric:"25.8 out 100 men",
					description:"of Asian ethnicity are estimated to develop Coronary Heart Disease between the age of 35 and 65"
				},
				factors:{
					age:"35 - 65",
					ancestry:"Asian",
					sex:"Male",
					snp:"rs10757278 (9p21 region), rs12526453 (PHACTR1),rs17468148 (CXCL12), rs1122608 (SMARCA4), rs12526453 (PHACTR1), rs17468148 (CXCL12), rs1122608 (SMARCA4), rs12526453 (PHACTR1), rs17468148 (CXCL12)"
				}
			},
			lab_tests:{
				tests:[
					{title:"Cholesterol",value:"198 mg/dL", description:"Ideal is <200 mg/dL"},
					{title:"HDL",value:"68 mg/dL", description:"Ideal is >40 mg/dL"},
					{title:"LDL",value:"120 mg/dL", description:"Ideal is <100 mg/dL"},
					{title:"Triglyceride",value:"212 mg/dL", description:"Ideal is <149 mg/dL"},
				],
				trend:[
					{value:"",year:""},
					{value:"",year:""},
				],
				risks:[
					{title:"Hypertension", description: ""},
					{title:"Atrial Fibrillation", description: ""},
					{title:"Coronary Heart Disease", description: ""}
				]
			},
			family_history:{
				mother:{
					description:"Alive 69",
					grandpa:{
						description:"Deceased (Emphysema)",
					},
					grandma:{
						description:"Alive 91"
					}
				},
				father:{
					description:"Alive 70",
					grandpa:{
						description:"Deceased (Pancreatic Cancer)"
					},
					grandma:{
						description:"Deceased (Stomach Cancer)"
					}
				},
				risks:[
					{title:"Atrial Fibrillation", description:""},
					{title:"Coronary Heart Disease", description:""},
					{title:"Venous Tromboembolism", description:""},
				]
			},
		},
		medical_history:{
			medical_history:[
				{
					title:"2016",
					type:"year"
				},
				{
					title:"Physical Exam",
					type:"physical",
					datetime:"08/19/2016",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"Flu Vaccine",
					type:"medicine",
					datetime:"01/15/2016",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2016",
					type:"year"
				},
				{
					title:"Physical Exam",
					type:"physical",
					datetime:"05/03/2014",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"Strep Throat - Prescription (Amoxicillin 200mg 2x daily for 2 weeks)",
					type:"medicine",
					datetime:"08/26/2014",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2014",
					type:"year"
				},
				{
					title:"Physical Exam",
					type:"physical",
					datetime:"08/19/2012",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2012",
					type:"year"
				},
				{
					title:"Broken Wrist (Cast for 4 weeks, Physical therapy 3x week for 8 weeks)",
					type:"medicine",
					datetime:"02/11/2010",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2010",
					type:"year"
				},
				{
					title:"Cyst Removal",
					type:"surgery",
					datetime:"05/03/2009",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2009",
					type:"year"
				},
				{
					title:"Physical Exam",
					type:"physical",
					datetime:"08/26/2007",
					description:"Martin Luther King, Jr. Community Hospital",
					
				},
				{
					title:"2007",
					type:"year"
				},
			]
		}
	});
  
};

